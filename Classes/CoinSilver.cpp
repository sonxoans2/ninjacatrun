#include "CoinSilver.h"
#define DRAW_COLLIDE_COIN_BOX 0

CoinSilver::CoinSilver(void)
{
}


CoinSilver::~CoinSilver(void)
{
}

bool CoinSilver::init(/*int width*/) {
	//this->width = width;
	auto *c = Sprite::create("bac.png");
	//float lx = c->getContentSize().width; 
	/*for(int i = 0; i < width; i++) {
		c = Sprite::create("bac.png");
		c->setPosition(Point(lx, 0));
		this->addChild(c);

		lx += c->getContentSize().width;
	}*/
	this->addChild(c);
	setContentSize(Size(c->getContentSize().width, c->getContentSize().height));
#if DRAW_COLLIDE_COIN_BOX
	Rect rect = getRectCoin();
	DrawNode *dn = DrawNode::create();
	dn->drawSegment(Point(-rect.size.width/2, rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED); //tren
	dn->drawSegment(Point(-rect.size.width/2, -rect.size.height/2), Point(-rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED); //trai
	dn->drawSegment(Point(rect.size.width/2, -rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED); //phai
	dn->drawSegment(Point(-rect.size.width/2, -rect.size.height/2), Point(rect.size.width/2, -rect.size.height/2), 2.0f, Color4F::RED);//duoi
	this->addChild(dn, 1);
#endif
	return true;
}

CoinSilver *CoinSilver::create(/*int width*/) {
	CoinSilver * c = new CoinSilver();
	c->init(/*width*/);
	c->autorelease();
	c->setCoinCategory(coinSilverCategory);
	return c;
}

Rect CoinSilver::getRectCoin() {
	Rect rect;
	rect.size = Size(20, 20);
	rect.origin = Point(_position.x - rect.size.width/2, _position.y - rect.size.height/2);
	return rect;
}
