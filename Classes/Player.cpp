#include "Player.h"
#include "GameScene.h"

#define DRAW_COLLIDE_PLAYER_BOX 0

Player::Player() {
}

bool Player::init() {
	Armature::init();
	_currentAction = nullptr;
	this->playerRun();

// 	_playerG->getAnimation()->setMovementEventCallFunc([=](Armature *armature, MovementEventType movementType, const std::string& movementID){
// 		this->animationEvent(armature, movementType, movementID);
// 	});

// 	_playerG->getAnimation()->setMovementEventCallFunc(std::bind(&Player::animationEvent, this, 
// 																		std::placeholders::_1, 
// 																		std::placeholders::_2, 
// 																		std::placeholders::_3));

	return true;
}

Armature *Player::playerRun() {
	_playerG = Armature::create("anicat");
	_playerG->getAnimation()->playWithIndex(0);
	this->addChild(_playerG);
#if DRAW_COLLIDE_PLAYER_BOX
	Rect rect = getCollideRect();
	DrawNode *dn = DrawNode::create();
	dn->drawSegment(Point(-rect.size.width/2, rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED); //tren
	dn->drawSegment(Point(-rect.size.width/2, -rect.size.height/2), Point(-rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED); //trai
	dn->drawSegment(Point(rect.size.width/2, -rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED); //phai
	dn->drawSegment(Point(-rect.size.width/2, -rect.size.height/2), Point(rect.size.width/2, -rect.size.height/2), 2.0f, Color4F::RED);//duoi
	this->addChild(dn, 1);
#endif
	return _playerG;
}

// void Player::animationEvent(Armature *armature, MovementEventType movementType, const std::string& movementID) {
// 	if (movementType == COMPLETE && movementID == "nhay") {
// 
// 	}
// }

void Player::handleTouchBegin() {
	switch(_state) {
	case StateNone:
		break;
	
	case StateRun:
		this->setState(StateJump);
		break;
	
	case StateJump:
		this->setState(StateThrowRope);
		break;
	
	case StateThrowRope:
		break;
	
	case StateFly:
		break;
	
	case StateDie:
		break;
	}
}


State Player::getState() {
	return _state;
}

void Player::update(float delta) {
	switch(_state) {
	case StateNone:
		break;

	case StateRun:

		break;

	case StateJump:
		_currentAction->step(delta);
		if (_currentAction->isDone())
		{
			this->setState(StateRun);
		}
		break;

	case StateThrowRope:
		_currentAction->step(delta);
		if (_currentAction->isDone())
		{
			this->setState(StateRun);
		}
		break;

	case StateFly:
		_currentAction->step(delta);
		if (_currentAction->isDone())
		{
			this->setState(StateRun);
		}
		break;

	case StateDie:
		_currentAction->step(delta);
		if (_currentAction->isDone())
		{
			this->setState(StateNone);
		}
		break;
	}
}

void Player::setState(State newState) {
	_state = newState;
	switch(_state) {
	case StateNone:

		break;
	case StateRun:
		_playerG->getAnimation()->playWithIndex(0);
		break;
	case StateJump:
		CC_SAFE_RELEASE(_currentAction);				
		_currentAction = CCJumpTo::create(0.8f, getPosition(), 120, 1);
		_currentAction->startWithTarget(this);
		_currentAction->retain();

		_playerG->getAnimation()->playWithIndex(1);
		break;
	case StateThrowRope:
		{
			// Add hammer
			_game->addHammer(_position + Point(20, 20), CC_DEGREES_TO_RADIANS(45));
		}
		break;
	
	case StateFly:
		CC_SAFE_RELEASE(_currentAction);				
		_currentAction = JumpTo::create(1.0f, getPosition() - Point(0, 120), 120, 1);
		_currentAction->startWithTarget(this);
		_currentAction->retain();

		_playerG->getAnimation()->playWithIndex(3);
		break;
	case StateDie:
		CC_SAFE_RELEASE(_currentAction);	
		_currentAction = JumpTo::create(0.5f, getPosition() + Point(50, 50), -120, 1);
		_currentAction->startWithTarget(this);
		_currentAction->retain();

		_playerG->getAnimation()->playWithIndex(4);
		break;
	}
}

Rect Player::getCollideRect() {
	Rect rect;
	rect.size = Size(50, 70);
	rect.origin = Point(_position.x - rect.size.width/2, _position.y - rect.size.height/2);
	return rect;
}