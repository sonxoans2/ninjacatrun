#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "cocostudio/CocoStudio.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocostudio;
using namespace CocosDenshion;


AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("Funny");
		glview->setFrameSize(640, 480);
		//glview->setDesignResolutionSize(640, 480, ResolutionPolicy::EXACT_FIT);
        director->setOpenGLView(glview);
    }

	std::vector<std::string> searchPaths;
	searchPaths.push_back("images");
	searchPaths.push_back("anicat");
	searchPaths.push_back("buddha");
	searchPaths.push_back("fonts");
	searchPaths.push_back("musics");
	FileUtils::getInstance()->setSearchPaths(searchPaths);

	ArmatureDataManager::getInstance()->addArmatureFileInfo("anicat0.png", "anicat0.plist", "anicat.ExportJson");
	ArmatureDataManager::getInstance()->addArmatureFileInfo("buddha0.png", "buddha0.plist", "buddha.ExportJson");

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    auto scene = HelloWorld::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
     SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
     SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
