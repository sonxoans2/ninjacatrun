﻿#include "House.h"
//static float widthHouse = 166;

#define DRAW_COLLIDE_BOX 0

House::House(void) {
}


House::~House(void) {
}

// int House::getWidthHouse() {
// 	return width;
// }
// 
// void House::setWidthHouse(int newWidth) {
// 	width = newWidth;
// }

bool House::init(int newWidth) {
	width = newWidth;
	if (TerrainBase::init()) {
		auto *s = Sprite::create("nha2.png");

		float contentSizeWidth = (newWidth - (newWidth - 1) * 0.3) * s->getContentSize().width; 
		float lx = -contentSizeWidth/2;
		for (int i = 0; i < newWidth; i++)	{
			s = Sprite::create("nha2.png");
			s->setAnchorPoint(Point(0, 0.5));
			s->setPosition(Point(lx, 0));
			this->addChild(s);

			lx += s->getContentSize().width*0.7f;
		}

		setContentSize(Size(contentSizeWidth, s->getContentSize().height));
	}

#if DRAW_COLLIDE_BOX
	Rect rect = getCollideRect();
	DrawNode *n = DrawNode::create();
	n->drawSegment(Point(-rect.size.width/2, rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED);
	n->drawSegment(Point(-rect.size.width/2, rect.size.height/2), Point(-rect.size.width/2, -rect.size.height/2), 2.0f, Color4F::RED);
	this->addChild(n, 1);
#endif

	return false;
}

House *House::create(int newWidth) {
	House *h = new House();
	h->init(newWidth);
	h->autorelease();
	h->setCollideCategory(CollideCategoryHouse);
	return h;
}

Rect House::getCollideRect() {
 	Rect mRect;
	mRect.size = Size(_contentSize.width - 70, _contentSize.height);
	mRect.origin = Point(_position.x - mRect.size.width/2, _position.y - mRect.size.height/2);
	return mRect;
}