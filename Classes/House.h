#ifndef __HOUSE_H__
#define __HOUSE_H__
#include "cocos2d.h"
#include "TerrainBase.h"

USING_NS_CC;

class House : public TerrainBase {
private:
		int width;
public:

// 	int getWidthHouse();	
// 	void setWidthHouse(int newWidth);

	House(void);
	~House(void);

	bool init(int width);
	static House *create(int width);
	Rect getCollideRect();
	DrawNode *_drawNode;
};
#endif //__HOUSE_H__