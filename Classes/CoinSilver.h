#ifndef __COINT_H__
#define __COINT_H__
#include "cocos2d.h"
#include "CoinBase.h"

USING_NS_CC;

class CoinSilver : public CoinBase {
// private:
// 	int width;
public:
	CoinSilver(void);
	~CoinSilver(void);

	bool init(/*int width*/);
	static CoinSilver *create(/*int width*/);

	Rect getRectCoin();

};
#endif //__COINT_H__
