#ifndef __TOWER_H__
#define __TOWER_H__
#include "cocos2d.h"
#include "TerrainBase.h"

USING_NS_CC;

class Tower : public TerrainBase {
public:
	static Tower *create();
	Rect getCollideRect();

	bool init();
};
#endif //__TOWER_H__