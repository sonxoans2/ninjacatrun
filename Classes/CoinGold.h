#ifndef __COINTGOLD_H__
#define __COINTGOLD_H__
#include "cocos2d.h"
#include "CoinBase.h"

USING_NS_CC;

class CoinGold : public CoinBase {
// private:
// 	int width;
public:
	CoinGold(void);
	~CoinGold(void);

	bool init(/*int width*/);
	static CoinGold *create(/*int width*/);

	Rect getRectCoin();

};
#endif //__COINTGOLD_H__
