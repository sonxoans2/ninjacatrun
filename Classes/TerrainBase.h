#ifndef __TERRAIN_H__
#define __TERRAIN_H__
#include "cocos2d.h"

USING_NS_CC;

enum {
	CollideCategoryHouse = 1,
	CollideCategoryTower
};
class TerrainBase : public Node {
public:
	Rect getCollideRect();

	int getCollideCategory();
	void setCollideCategory(int newCollideCategory);

protected:
	int _collideCategory;

};
#endif //__TERRAIN_H__