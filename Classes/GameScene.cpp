﻿#include "GameScene.h"
#include "HelloWorldScene.h"
#include "TerrainBase.h"
#include "House.h"
#include "CoinGold.h"
#include "CoinSilver.h"
#include "Tower.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

#include "cocos-ext.h"
#include "cocostudio/CocoStudio.h"

static float house_gap_min = 150.0;
static float house_gap_max = 200.0;
static int house_width_min = 3;
static int house_width_max = 7;

static float SPEED = 600;

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocostudio;
using namespace std;
using namespace CocosDenshion;
int *heightCoin = new int[];

int posHeightCoin1[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
int posHeightCoin2[] = {1, 1, 1, 1, 2, 3, 4, 4, 3, 2, 1, 1, 1, 1, 1};
int posHeightCoin3[] = {1, 1, 1, 1, 2, 3, 4, 3, 2, 1, 1, 1, 1, 1, 1};
int posHeightCoin4[] = {1, 1, 1, 2, 3, 3, 4, 4, 3, 3, 2, 1, 1, 1, 1};
int posHeightCoin5[] = {1, 1, 1, 2, 3, 2, 1, 2, 3, 4, 3, 2, 1, 1, 1};

int heightCoinIndex = 0;
int heightCoinIndexMax = 15;
bool heightCoinIndexEnd = false;

GameScene::~GameScene(void) {
}

Scene *GameScene::createGameScene() {
	auto scene = Scene::create();
	auto layer = GameScene::create();

	scene->addChild(layer);

	return scene;
}

bool GameScene::init() {
	if ( !Layer::init())
	{
		return false;
	}

	SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
	_setSound = UserDefault::getInstance()->getIntegerForKey("sound");
	if (_setSound == 1)
	{
		//_isSound = false;
		SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
	} else {
		//_isSound = true;
		std::string _filePath = FileUtils::getInstance()->fullPathForFilename("musics/gamebg.mp3");
		SimpleAudioEngine::getInstance()->playBackgroundMusic(_filePath.c_str());
	}

	/*if (!_isSound)
	{
		SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
	}else {
		std::string _filePath = FileUtils::getInstance()->fullPathForFilename("musics/gamebg.mp3");
		SimpleAudioEngine::getInstance()->playBackgroundMusic(_filePath.c_str());
	}*/
	_screenSize = Director::getInstance()->getWinSize();
	_state = State::PLAYING;

	_speed = SPEED;
	_score = 0;
	_hightScore = UserDefault::getInstance()->getIntegerForKey("hightScore");
	heightCoin = posHeightCoin1;


	this->addBackGroundGame();
	this->addMenuGame();
	this->addPlayer();
	this->addBuddha();
	this->addLabelScore();

	this->addTerrain(0, 5);

	for (int i = 0; i < heightCoinIndexMax; i++)
	{
		addCoin(_screenSize.width, i, 1, true);
	}

	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->schedule(schedule_selector(GameScene::update));
	return true;
}

void GameScene::addBackGroundGame() {
	auto *bg = Sprite::create("bgGame.jpg");
	bg->setPosition(Point(_screenSize.width/2, _screenSize.height/2));
	this->addChild(bg);
}

void GameScene::addBuddha() {
	_buddha = Armature::create("buddha");
	_buddha->getAnimation()->playWithIndex(0);
	_buddha->setPosition(Point(_screenSize.width * 0.5f, _screenSize.height *0.15f));
	this->addChild(_buddha);

}

void GameScene::addLabelScore() {

	char score[100]={0};
	sprintf(score, "%d", _score);

	_labelScore = LabelBMFont::create("0", "fontscore.fnt");
	_labelScore->setPosition(Point(_screenSize.width *0.1f, _screenSize.height * 0.9f));
	//_labelScore->setString(score);	
	this->addChild(_labelScore);


	sprintf(score, "%d", _hightScore);
	_labelHightScore = LabelBMFont::create("0", "font.fnt");
	_labelHightScore->setPosition(Point(_screenSize.width *0.3f, _screenSize.height * 0.55f));
	_labelHightScore->setScale(0.6f);
	_labelHightScore->setString(score);	
	this->addChild(_labelHightScore);
}

void GameScene::addMenuGame() {
	auto *_menu = Menu::create();
	_menu->setPosition(Point(0,0));
	this->addChild(_menu, 9);

	auto *_back = MenuItemImage::create("button_on.png", "button_off.png");
	_back->setPosition(Point(_screenSize.width*0.8f, _screenSize.height*0.8f));
	_back->setTag(1);
	_back->setCallback(std::bind((SEL_MenuHandler) &GameScene::onMenuActiveItemGame, this, std::placeholders::_1));
	_menu->addChild(_back, 45);
}

void GameScene::onMenuActiveItemGame(MenuItem * item) {
	switch(item->getTag()) {
	case 1:
		auto transition = TransitionJumpZoom::create(0.5f, HelloWorld::createScene());
		Director::getInstance()->replaceScene(transition);
		break;
	}
}

void GameScene::addPlayer() {
	_ninjaCat = new Player;
	_ninjaCat->init();
	_ninjaCat->setState(StateRun);
	_ninjaCat->setPosition(Point(_screenSize.width * 0.3f, _screenSize.height * 0.35f));
	_ninjaCat->setGame(this);
	this->addChild(_ninjaCat, 999);

}

void GameScene::update(float dt) {
				
	switch (_state) {
	case State::PLAYING:
		{
			this->updateTerrain(dt);
			this->updateCoin(dt);
			_ninjaCat->update(dt);
			this->updateHammers(dt);
			collisionHouse(dt);
			collosionCoin(dt);
			break;
		}
	case State::OVER:
		{
			_ninjaCat->setState(StateDie);
			_ninjaCat->update(dt);

			if (_score > _hightScore)
			{
				_hightScore = _score;
				UserDefault::getInstance()->setIntegerForKey("hightScore", _hightScore);
// 				char score[100]={0};
// 				sprintf(score, "%d", _hightScore);
// 				_labelHightScore->setString(score);
			}

		break;
		}
	}

	char score[100]={0};
	sprintf(score, "%d", _score);
	_labelScore->setString(score);

	if (0 == _score % 500)
	{
		_speed = SPEED * 1.2;
	}

	/*if (_score == _hightScore *0.5f)
	{
		_buddha->getAnimation()->playWithIndex(1);
	}else if (_score > _hightScore && _score < 2000)
	{
		_buddha->getAnimation()->playWithIndex(2);
	}*/
	
}

void GameScene::updateTerrain(float dt) {
	for (int i = 0; i < _terrains2.size();) {
		House *t = (House*) _terrains2[i];
		
		//Move
		t->setPositionX(t->getPositionX() - _speed * dt);
		
		//Remove
		if (t->getPositionX() + t->getContentSize().width/2 < 0) {
			_terrains2.erase(_terrains2.begin() + i);
			t->removeFromParentAndCleanup(true);
			log("=======================================================remove");
		}else {
			i++;
		}
	}

	//Add
	House *t = (House *) _terrains2.back();
	float rx = t->getPositionX() + t->getContentSize().width/2;
	if (rx < _screenSize.width + 20) {
		if (CCRANDOM_0_1() > 0.8) {
			//Add tower
			log("=====================================================add tower");
			Tower *_tower = Tower::create();
			_tower->setPosition(Point(rx + _tower->getContentSize().width * 0.7f, 220));
			this->addChild(_tower);
			_terrains2.push_back(_tower);

			//Add House
			log("======================================================add cloud");
			int width = (house_width_max - house_width_min) * CCRANDOM_0_1() + house_width_min;
			addTerrain(_tower->getPositionX() + _tower->getContentSize().width * 0.7f, width);
		}else {
			//Add House
			log("======================================================add cloud");
			float gap = (house_gap_max - house_gap_min) * CCRANDOM_0_1() + house_gap_min;

			int width = (house_width_max - house_width_min) * CCRANDOM_0_1() + house_width_min;
			addTerrain(rx + gap, width);
			//addCoint(rx, width * 3);
		}
	}
}


void GameScene::updateHammers(float dt) {
	//for (Hammer *h : _hammers) {
	for (int i=0; i< _hammers.size(); i++)
	{
		Hammer *h = (Hammer*) _hammers.at(i);
			h->update(dt, -dt * _speed);

		if (h->getPositionY() > _screenSize.height + h->getContentSize().height/2 || h->getPositionX() < h->getContentSize().width/2)
		{
			removeHammer(h);			
		}
	}
}

Hammer *GameScene::addHammer(const Point &position, float angle) {
	Hammer *throw2 = Hammer::create(CC_DEGREES_TO_RADIANS(45));
	this->addChild(throw2);
	throw2->setPosition(position);
	_hammers.pushBack(throw2);
 	return throw2;
}

void GameScene::removeHammer(Hammer *hammer) {
	_hammers.eraseObject(hammer);
	hammer->removeFromParentAndCleanup(true);
	log("===========================remove Hammer");
}

void GameScene::collisionHouse(float dt) {
	switch(_ninjaCat->getState()){
	case StateRun:
		{	
			bool live = false;
			Rect r1 = _ninjaCat->getCollideRect();
			for (int i = 0; i < _terrains2.size(); i++) {
				House *t = (House*) _terrains2[i];

				Rect r2 = t->getCollideRect();
			if (t->getCollideCategory() == CollideCategoryHouse)
			{
				if (r1.origin.x < r2.origin.x + r2.size.width && r1.origin.x + r1.size.width > r2.origin.x){
					live = true;
					break;
				}
			}
			}
			if (!live) {
		
				_state = State::OVER;
			}

			if (_ninjaCat->getPositionY() <= _screenSize.height * 0.35f)
			{
				_ninjaCat->setPositionY(_screenSize.height * 0.35f);
			}
		}
	break;

	case StateThrowRope:
	//	bool cham = false;
		for (int i = 0; i < _terrains2.size(); i++) {
			Tower *t = (Tower *) _terrains2[i];
			for (int j = 0; j < _hammers.size(); j++) {
				Hammer *h = (Hammer*) _hammers.at(j);

				Rect t1 = t->getCollideRect();
				Rect h1 = h->getCollideRectHammer();
				if (t->getCollideCategory() == CollideCategoryTower)
				{
					if (h1.intersectsRect(t1) /*ccpDistance(t->getPosition(), h->getPosition()) < 45*/)
					{
						log("------------------------------------Cham");
						_ninjaCat->setState(StateFly);
						//cham = true;
						//break;
					}
				}
			}
		}
	break;
		
	}
}

void GameScene::addTerrain(float posX, int width) {
	House * h = House::create(width);
	h->setPosition(Point(posX + h->getContentSize().width/2, 50));
	this->addChild(h);

	_terrains2.push_back(h);
}

void GameScene::addCoin(float posX, int width, int height, bool isCoin) {

// 	for (int i1 = 0; i1 < width; i1++)
// 	{
 		if (!isCoin)
 		{
 				CoinGold *c = CoinGold::create(/*width*/);
 				c->setPosition(Point(posX + width * c->getContentSize().width, _screenSize.height * 0.2f  + height * c->getContentSize().height));
 				this->addChild(c, 9);
 				_coins.push_back(c);
 		}else {
				CoinSilver *c = CoinSilver::create(/*width*/);
				c->setPosition(Point(posX + width * c->getContentSize().width, _screenSize.height * 0.2f  + height * c->getContentSize().height));
				this->addChild(c, 9);
				_coins.push_back(c);
		}
	//}
}


void GameScene::updateCoin(float dt) {
	for (int i = 0; i < _coins.size();) {
		CoinBase *c = (CoinBase *) _coins[i];

		//Move
		c->setPositionX(c->getPositionX() - _speed * dt);

		//Remove
		if (c->getPositionX() + c->getContentSize().width/2 < 0) {
			_coins.erase(_coins.begin() + i);
			c->removeFromParentAndCleanup(true);
			log("=======================================================remove Coin");
		}else {
			i++;
		}
	}
	
	//Add
	if (heightCoinIndexEnd)
	{
 		switch(1+ rand()%4){
 		case 1:
 			heightCoin = posHeightCoin1;
 		break;
 		case 2:
 			heightCoin = posHeightCoin2;
 		break;
 		case 3:
 			heightCoin = posHeightCoin3;
 		break;
		case 4:
			heightCoin = posHeightCoin4;
		break;
		case 5:
			heightCoin = posHeightCoin5;
		break;
 		}
	}
	CoinBase *t = (CoinBase *) _coins.back();
	float rx = t->getPositionX() + t->getContentSize().width;
	if (rx < _screenSize.width + 20) {
		if(CCRANDOM_0_1() > 0.7f){
			for (int i = 0; i < heightCoinIndexMax; i++)
			{
				addCoin(rx, i, heightCoin[heightCoinIndex], false);
				heightCoinIndex++;
				if (heightCoinIndex == heightCoinIndexMax)
				{
					heightCoinIndex = 0;
					heightCoinIndexEnd = true;
				}else heightCoinIndexEnd = false;
			}
		}else{
		log("======================================================add coin");
		for (int i = 0; i < heightCoinIndexMax; i++)
		{
			addCoin(rx, i, heightCoin[heightCoinIndex], true);
			heightCoinIndex++;
			if (heightCoinIndex == heightCoinIndexMax)
			{
				heightCoinIndex = 0;
				heightCoinIndexEnd = true;
			}else heightCoinIndexEnd = false;
		}
		}


		
	}
}

void GameScene::collosionCoin(float dt) {
	Rect r1 = _ninjaCat->getCollideRect();
	for (int i = 0; i < _coins.size();)
	{
		CoinSilver * c = (CoinSilver*) _coins[i];
		Rect c1 = c->getRectCoin();
		if (r1.intersectsRect(c1))
		{
			if(c->getCoinCategory() == coinSilverCategory){
				_score++;
				log("========----------------------bac +1");
			}else {
				_score += 2;
				log("========----------------------vang +2");
				}

			_coins.erase(_coins.begin() + i);
			c->removeFromParentAndCleanup(true);
			log("========----------------------an tien");
		}else {
			i++;
		}
	}
}

bool GameScene::onTouchBegan(Touch *touch, Event *unused_event) {
	_ninjaCat->handleTouchBegin();
 	return true;
}