#include "HelloWorldScene.h"
#include "GameScene.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
USING_NS_CC;
using namespace CocosDenshion;


Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	_setSound = UserDefault::getInstance()->getIntegerForKey("sound");
 	if (_setSound == 1)
 	{
 		_isSound = false;
 	} else {
 		_isSound = true;
 	}
	
	_screenSize = Director::getInstance()->getWinSize();
    addBackground();
	addMenu();
    return true;
}

void HelloWorld::addBackground() {
	auto *bg = Sprite::create("bgMenu.png");
	bg->setPosition(Point(_screenSize.width/2, _screenSize.height/2));
	this->addChild(bg);

	//auto *test = Sprite::create("thap.png", Rect(0, -0, 230, 90));
	//test->setPosition(Point(_screenSize.width/2, _screenSize.height/2));
	//Rect _rect = test->getBoundingBox();
	//_rect = CCRectMake(_rect.origin.x + 200, _rect.origin.y + 200, _rect.size.width, _rect.size.height * 0.2f);
	//this->addChild(test);

	if (!_isSound)
	{
		SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
	}else {
		std::string _filePath = FileUtils::getInstance()->fullPathForFilename("musics/titlebg.mp3");
		SimpleAudioEngine::getInstance()->playBackgroundMusic(_filePath.c_str());
	}

}

void HelloWorld::addMenu() {
	auto *_menu = Menu::create();
	_menu->setPosition(Point(0,0));
	this->addChild(_menu, 9);


	auto *_play = MenuItemSprite::create(Sprite::create("button_play.png"), 
		Sprite::create("button_play_bat.png"));

	_play->setScale(0.5f);
	_play->setPosition(Point(_screenSize.width*0.7f, _screenSize.height*0.3f));
	_play->setCallback(std::bind((SEL_MenuHandler)&HelloWorld::onMenuItemActivated, this, std::placeholders::_1));
	_play->setTag(1);
	_menu->addChild(_play, 45);

	if (_isSound)
	{
		_sound = MenuItemSprite::create(Sprite::create("button_on.png"), 
			Sprite::create("button_off.png"));
	}else{
		_sound = MenuItemSprite::create(Sprite::create("button_off.png"), 
			Sprite::create("button_on.png"));
	}
	_sound->setScale(0.5f);
	_sound->setPosition(Point(_screenSize.width*0.85f, _screenSize.height*0.8f));
	_sound->setCallback(std::bind((SEL_MenuHandler)&HelloWorld::onSettingSound, this, std::placeholders::_1));
	_sound->setTag(1);
	_menu->addChild(_sound, 45);


}

void HelloWorld::onMenuItemActivated(MenuItem* item) {
	switch (item->getTag()) {
	case 1:
		auto transition = TransitionJumpZoom::create(0.5, GameScene::createGameScene());
		Director::getInstance()->replaceScene(transition);
	break;
	}
}

void HelloWorld::onSettingSound(MenuItem* item) {
	switch (item->getTag()) {
	case 1:
		{
			Sprite *frame;
			Rect rect = CCRectZero;
			rect.size = _sound->getContentSize();

			if (_isSound)
			{
				_isSound = false;
				UserDefault::getInstance()->setIntegerForKey("sound", 1);
				SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);

				frame = Sprite::create("button_off.png", rect);
				_sound->setNormalImage(frame);
			}else {
				_isSound = true;
				UserDefault::getInstance()->setIntegerForKey("sound", 2);
				std::string _filePath = FileUtils::getInstance()->fullPathForFilename("musics/titlebg.mp3");
				SimpleAudioEngine::getInstance()->playBackgroundMusic(_filePath.c_str());
				frame = Sprite::create("button_on.png", rect);
				_sound->setNormalImage(frame);
			}
		}
		
	break;
	}
}

