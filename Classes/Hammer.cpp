#include "Hammer.h"

#define DRAW_COLLIDE_HAMMER_BOX 1
//Hammer::Hammer(float angle): _angle(angle)
//{
// 	Sprite *hammer = Sprite::create("daygan.png");
// 	hammer->setRotation(angle);
// 	this->addChild(hammer);
//}

Hammer *Hammer::create(float angle) {
		Hammer *h = new Hammer();
		h->init(angle);
		h->setRotation(CC_RADIANS_TO_DEGREES(-angle));
		return h;
}

bool Hammer::init(float angle) {
	_angle = angle;
	Sprite *_hammer2 = Sprite::create("daygan.png");
	this->addChild(_hammer2);

#if DRAW_COLLIDE_HAMMER_BOX
	Rect rect = getCollideRectHammer();
	DrawNode *dn = DrawNode::create();
	dn->drawSegment(Point(0, rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED); //tren
	dn->drawSegment(Point(0, -rect.size.height/2), Point(0, rect.size.height/2), 2.0f, Color4F::RED);//trai
	dn->drawSegment(Point(rect.size.width/2, -rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED);//phai
	dn->drawSegment(Point(0, -rect.size.height/2), Point(rect.size.width/2, -rect.size.height/2), 2.0f, Color4F::RED);//duoi
	this->addChild(dn, 1);
#endif

	return true;
}


void Hammer::update(float dt, float dx) {
	float r = 20;
	Point pos = _position + Point(cosf(_angle), sinf(_angle)) * r;
	pos.x += dx;
	//this->setScaleX(1+dx);
	setPosition(pos);
	//setRotation(CC_DEGREES_TO_RADIANS(-_angle));
}

Rect Hammer::getCollideRectHammer() {
	Rect rect;
	rect.size = Size(50, 20);
	rect.origin = Point(_position.x - rect.size.width/2, _position.y - rect.size.height/2);
	 return rect;
}

