#ifndef __COINBASE_H__
#define __COINBASE_H__
#include "cocos2d.h"

USING_NS_CC;
enum {
	coinSilverCategory = 1,
	coinGoldCategory
};
class CoinBase : public Node
{
public:
	Rect getRectCoin();

	int getCoinCategory();
	void setCoinCategory(int coinCategory);

protected:
	int _coinCategory;
};

#endif //__COINBASE_H__