#include "Tower.h"
#define DRAW_COLLIDE_TOWER_BOX 1

Tower *Tower::create() {
	Tower *_tower = new Tower;
	//if (_tower) {
// 		Sprite *t = Sprite::create("thap2.png");
// 		_tower->addChild(t);
		_tower->init();
		_tower->autorelease();
//		_tower->setContentSize(t->getContentSize());
		_tower->setCollideCategory(CollideCategoryTower);
		return _tower;
//	}

//	CC_SAFE_DELETE(_tower);
//	return NULL;
}

bool Tower::init() {
	Sprite *t = Sprite::create("thap.png");
	this->addChild(t);
	setContentSize(t->getContentSize());

#if DRAW_COLLIDE_TOWER_BOX
	Rect rect = getCollideRect();
	DrawNode *n = DrawNode::create();
// 	n->drawSegment(Point(-rect.size.width/2, rect.size.height/2), Point(rect.size.width/2, rect.size.height/2), 2.0f, Color4F::RED);
// 	n->drawSegment(Point(-rect.size.width/2, rect.size.height/2), Point(-rect.size.width/2, -rect.size.height/2), 2.0f, Color4F::RED);
// 	n->drawSegment(Point(rect.size.width/2, rect.size.height/2), Point(rect.size.width/2, -rect.size.height/2), 2.0f, Color4F::RED);
// 	n->drawSegment(Point(-rect.size.width/2, -rect.size.height/2), Point(rect.size.width/2, -rect.size.height/2), 2.0f, Color4F::RED);
	//n->drawSegment(Point(-230, 95), Point(230, 95), 2.0f, Color4F::RED);

	this->addChild(n, 1);
#endif

	return true;
}

Rect Tower::getCollideRect() {
	Rect mRect;
	mRect.size = Size(_contentSize.width/2, _contentSize.height/2);
	mRect.origin = Point(_position.x, _position.y - mRect.size.height/2);

	//mRect.size = Size(230, 95);
	//mRect.origin = Point(_position.x - mRect.size.width/2, _position.y - mRect.size.height/2);
	return mRect;
}