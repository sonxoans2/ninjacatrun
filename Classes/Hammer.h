
#include <iostream>
#include <cocos2d.h>

USING_NS_CC;

class Hammer : public Node
{
public:
// 	Hammer(float angle);
// 	~Hammer();

	static Hammer *create(float angle);

	bool init(float angle);

	void update(float dt, float dx);

	Rect getCollideRectHammer();

private:
	float _angle;	// Radian

};

