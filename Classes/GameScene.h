#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
//#include "cocostudio/CocoStudio.h"
#include "Player.h"
#include "Hammer.h"

USING_NS_CC;
using namespace std;
//using namespace cocostudio;
class GameScene : public cocos2d::Layer {
public:
	enum class State{
		NONE,
		PAUSED,
		PLAYING,
		OVER
	};

	~GameScene(void);

	static Scene *createGameScene();
	virtual bool init();

	CREATE_FUNC(GameScene);

	Size _screenSize;

	void addBackGroundGame();
	void addMenuGame();
	void onMenuActiveItemGame(MenuItem * item);
	void addPlayer();

	void addBuddha();
	void addLabelScore();

	bool GameOver;
	void resetGame();

	Hammer *addHammer(const Point &position, float angle);
	void removeHammer(Hammer *hammer);

private:
		vector<Node*> _terrains2;
		vector<Node*> _coins;
		Vector<Hammer *> _hammers;

		void addTerrain(float posX, int width);
		void updateTerrain(float dt);
		void updateHammers(float dt);
		void collisionHouse(float dt);

		void addCoin(float posX, int width, int height, bool isCoin);
		void updateCoin(float dt);
		void collosionCoin(float dt);

		Player *_ninjaCat;
		Armature *_buddha;

		State _state;
		float _speed;

		int _score;
		int _hightScore;

		LabelBMFont *_labelScore;
		LabelBMFont *_labelHightScore;

		int _setSound;

		bool onTouchBegan(Touch *touch, Event *unused_event);
		virtual void update(float dt) override;
};

#endif //__GAME_SCENE_H__