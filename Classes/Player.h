#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "cocostudio/CocoStudio.h"

class GameScene;

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocostudio;

enum State{
	StateNone,
	StateRun,
	StateJump,
	StateThrowRope,
	StateFly,
	StateDie
};


class Player : public Armature {
public:
	Player();
	//~Player(void);

	bool init() override;

	CC_SYNTHESIZE(Armature*, _playerG, PlayerG);

	void animationEvent(Armature *armature, MovementEventType movementType, const std::string& movementID);

	//Armature *_playerG;

	void setGame(GameScene *game){
		_game = game;
	}

 	State getState();
	void setState(State newState);
 	void handleTouchBegin();
 
 	void update(float delta);

	Rect getCollideRect();
private:

	Armature *playerRun();
	State _state;
	GameScene *_game;
	ActionInterval *_currentAction;
};

#endif //__PLAYER_H__